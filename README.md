# Terraform

Terraform Cloud is used to manager all states changes and applies the changes to the providers configured on commit.

## Git Flow

Git Flow is utilised not just as a way of structuring environmental code changes but also because each Terraform workspace is setup to run on the according branch for its environment.

For example,

```
  master = Production
  staging = Staging
  develop = Development & testing
```

## Getting Started

This section should guide you through everything that you will need in order
to user Terraform and start managing infrastructure state.

### Installation

You will need to install Terraform CLI. There are two ways of installation.

* Using Homebrew `brew install terraform`.
* Downloading the CLI directly from [terraform.io/downloads.html](https://www.terraform.io/downloads.html).

### Development

Following best practises outlined by Terraform. Folders are structured like so,

```
Terraform directory structure

├── environments
│   ├── production
│   │   ├── main.tf
│   │   └── outputs.tf
│   │
│   ├── staging
│   │   ├── main.tf
│   │   └── outputs.tf
│   │
│   └── production
│       ├── main.tf
│       └── outputs.tf
│
└── modules
    ├── core
    │   ├── data.tf
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── variables.tf
    │
    ├── defaults
    │   ├── data.tf
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── variables.tf
    │
    └── * (etc...)

```

**Important**: Terraform commands MUST be ran within the desired environment / working directory. For example, `environments/production`. Each environment directory acts as `root` for the environment.

#### Documentation & reading

Documentation can be found on the [terraform.io](https://www.terraform.io/) website. Predefined modules can be found on the [registry.terraform.io](https://registry.terraform.io/) website.

#### Useful VS Code extensions

  * [Terraform (mauve.terraform)](https://marketplace.visualstudio.com/items?itemName=mauve.terraform) by Mikael Olenfalk - Syntax highlighting, linting, formatting, and validation for Hashicorp's Terraform.
  * [Terraform (4ops.terraform)](https://marketplace.visualstudio.com/items?itemName=4ops.terraform) by Anton Kulikov - Terraform configuration language support.
  * [Terraform doc snippets (run-at-scale.terraform-doc-snippets)](https://marketplace.visualstudio.com/items?itemName=run-at-scale.terraform-doc-snippets) by Run at Scale - Terraform code snippets (>3400) straight from documentation for all provider resources and data sources. All providers in the terraform-providers org covered.

### Commands

You will need to initialize the environment you wish to manager before any state
changes can be made.

`terraform init` - Initialize the Terraform working directory and Terraform Cloud (if not already initialized).

`terraform plan` - Generates and shows an execution plan of what the state will look like after changes are made.

`terraform validate` - Validates the Terraform files

`terraform help` - The available commands for execution.

### Outputs

Useful outputs will be displayed in [Terraform Cloud](https://app.terraform.io/app/Gravitywell/workspaces) (credentials held in 1Password) `Run` console logs.

#### Secrets management

As outputting secrets from the result of a `Terraform plan` or `Terraform apply` does not follow security best practises and is heavily frowned apon, sensitive data will be kept in secret managers depending on the selected provider. 99% of the time AWS Secrets Manager will be used to host these secrets.