output "ec2_availability_zone" {
  value = module.ec2.availability_zone
}

output "ec2_public_ip" {
  value = module.ec2.public_ip
}

output "ec2_security_group_id" {
  value = module.ec2.security_group_id
}

output "ec2_security_group_name" {
  value = module.ec2.security_group_name
}

output "rds_availability_zone" {
  value = module.rds.availability_zone
}

output "rds_security_group_id" {
  value = module.rds.security_group_id
}

output "rds_security_group_name" {
  value = module.rds.security_group_name
}

output "rds_instance_endpoint" {
  value = module.rds.instance_endpoint
}

output "rds_instance_db_name" {
  value = module.rds.instance_db_name
}

output "rds_instance_username" {
  value = module.rds.instance_db_name
}

output "rds_instance_port" {
  value = module.rds.instance_port
}
