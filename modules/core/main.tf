provider "aws" {
  profile = "default"
  region  = module.defaults.aws_region
}

module "defaults" {
  source = "../defaults"
}

module "rds" {
  source      = "../rds"
  environment = var.environment
  security_groups = [{
    description = module.ec2.security_group_name
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    id          = module.ec2.security_group_id
  }]
}

module "ec2" {
  source      = "../ec2"
  environment = var.environment
  security_groups = [{
    description = module.rds.security_group_name
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    id          = module.rds.security_group_id
  }]
}
