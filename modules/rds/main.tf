locals {
  security_group_name       = join("", [module.defaults.client_name, " - ", module.defaults.project_name, " RDS (", var.environment, ")"])
  db_secrets_manager_name   = join("", [module.defaults.client_project_machine_name, "-", var.environment, "-rds"])
  db_identifier             = join("", [module.defaults.client_project_machine_name, "-", var.environment])
  db_instance_class         = "db.t3.micro"
  db_engine                 = "mysql"
  db_major_engine_version   = "5.7"
  db_engine_version         = "5.7.26"
  db_storage_type           = "gp2"
  db_allocated_storage      = 20
  db_name                   = replace(module.defaults.client_project_short_machine_name, "-", "_")
  db_username               = replace(module.defaults.client_project_short_machine_name, "-", "_")
  db_port                   = "3306"
  db_create_db_subnet_group = false
  db_parameter_group_name   = "default.mysql5.7"
  db_parameter_group_family = "mysql5.7"
  db_option_group_name      = "default:mysql-5-7"
  db_monitoring_role_arn    = join("", ["arn:aws:iam::", module.defaults.aws_account_id, ":role/rds-monitoring-role"])
}

module "defaults" {
  source = "../defaults"
}

#
# Password generation
#
resource "random_password" "rds" {
  length           = 20
  min_upper        = 4
  min_lower        = 4
  min_numeric      = 4
  min_special      = 4
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

#
# AWS secrets
#
resource "aws_secretsmanager_secret" "rds" {
  name                    = local.db_secrets_manager_name
  recovery_window_in_days = 0
  tags                    = module.defaults.default_tags
}

# Initially creates the secret with no dependents
resource "aws_secretsmanager_secret_version" "rds_initial" {
  secret_id = aws_secretsmanager_secret.rds.id
  secret_string = jsonencode({
    host     = ""
    database = local.db_name
    username = local.db_username
    password = random_password.rds.result
    port     = local.db_port
  })
}

data "aws_secretsmanager_secret" "rds" {
  depends_on = [aws_secretsmanager_secret.rds]
  name       = local.db_secrets_manager_name
}

data "aws_secretsmanager_secret_version" "rds_initial" {
  secret_id = data.aws_secretsmanager_secret.rds.id
}

# Update the secret with the RDS host
resource "aws_secretsmanager_secret_version" "rds_update" {
  depends_on = [module.mysql_rds]
  secret_id  = aws_secretsmanager_secret.rds.id
  secret_string = jsonencode({
    host     = replace(module.mysql_rds.this_db_instance_endpoint, join("", [":", local.db_port]), "")
    database = local.db_name
    username = local.db_username
    password = jsondecode(data.aws_secretsmanager_secret_version.rds_initial.secret_string)["password"]
    port     = local.db_port
  })
}

#
# Create our RDS security group
#
module "mysql_rds_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = ">= 3.4.0"

  name        = local.security_group_name
  description = local.security_group_name
  vpc_id      = module.defaults.aws_vpc_id

  ingress_with_cidr_blocks = [
    {
      description = "MySQL GW office access"
      protocol    = "tcp"
      from_port   = 3306
      to_port     = 3306
      cidr_blocks = module.defaults.office_cidr
    }
  ]

  computed_ingress_with_source_security_group_id = [
    for security_group in var.security_groups :
    {
      description              = security_group.description
      protocol                 = security_group.protocol
      from_port                = security_group.from_port
      to_port                  = security_group.to_port
      source_security_group_id = security_group.id
    }
  ]

  number_of_computed_ingress_with_source_security_group_id = length(var.security_groups)

  egress_with_cidr_blocks = [
    {
      description = "All traffic"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

#
# Create our RDS instance
#
module "mysql_rds" {
  source  = "terraform-aws-modules/rds/aws"
  version = ">= 2.14.0"

  # Identifier / name
  identifier = local.db_identifier

  # DB engine
  engine            = local.db_engine
  engine_version    = local.db_engine_version
  instance_class    = local.db_instance_class
  storage_type      = local.db_storage_type
  allocated_storage = local.db_allocated_storage

  # DB credentials
  name     = local.db_name
  username = local.db_username
  password = jsondecode(data.aws_secretsmanager_secret_version.rds_initial.secret_string)["password"]
  port     = local.db_port

  # DB subnet group
  create_db_subnet_group = local.db_create_db_subnet_group
  publicly_accessible    = true

  # VPC subnet config
  availability_zone      = module.defaults.aws_availability_zone
  subnet_ids             = module.defaults.aws_subnet_ids
  vpc_security_group_ids = [module.mysql_rds_sg.this_security_group_id]

  # DB parameter group
  parameter_group_name = local.db_parameter_group_name
  family               = local.db_parameter_group_family

  # DB option group
  option_group_name    = local.db_option_group_name
  major_engine_version = local.db_major_engine_version

  # Tags
  tags                  = merge(module.defaults.default_tags, { Name = local.security_group_name })
  copy_tags_to_snapshot = true

  # DB maintenance
  maintenance_window         = "Sun:02:00-Sun:03:00"
  auto_minor_version_upgrade = true

  # DB backup and retention
  backup_retention_period = var.environment == "production" ? 7 : 1
  backup_window           = "03:30-05:30"

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  enabled_cloudwatch_logs_exports = var.environment == "production" ? ["general", "error", "slowquery"] : []
  monitoring_interval             = var.environment == "production" ? 10 : 0
  monitoring_role_arn             = local.db_monitoring_role_arn

  # Snapshot name upon DB deletion
  final_snapshot_identifier = join("", [local.db_identifier, "-final"])

  # Apply changes immediately
  apply_immediately = true

  # Database Deletion Protection
  deletion_protection = true
}
