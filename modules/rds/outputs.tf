output "availability_zone" {
  value = module.mysql_rds.this_db_instance_availability_zone
}

output "security_group_id" {
  value = module.mysql_rds_sg.this_security_group_id
}

output "security_group_name" {
  value = module.mysql_rds_sg.this_security_group_name
}

output "instance_endpoint" {
  value = replace(module.mysql_rds.this_db_instance_endpoint, join("", [":", local.db_port]), "")
}

output "instance_db_name" {
  value = local.db_name
}

output "instance_username" {
  value = module.mysql_rds.this_db_instance_username
}

output "instance_port" {
  value = module.mysql_rds.this_db_instance_port
}
