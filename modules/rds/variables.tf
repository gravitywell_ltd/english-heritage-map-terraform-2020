variable "environment" {
  type = string
}

variable "security_groups" {
  type = list(object({
    description = string
    protocol    = string
    from_port   = number
    to_port     = number
    id          = string
  }))
  default = []
}

