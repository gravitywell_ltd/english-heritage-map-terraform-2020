locals {
  # Client and project
  client_name                       = "English Heritage"
  project_name                      = "Voices of England Map"
  client_project_short_machine_name = "eh-voe"

  # Machine names (alpha numeric hyphenated)
  client_name_machine_name    = replace(replace(lower(local.client_name), "/[^A-Za-z0-9_ ]/", ""), "/[ ]/", "-")
  project_machine_name        = replace(replace(lower(local.project_name), "/[^A-Za-z0-9_ ]/", ""), "/[ ]/", "-")
  client_project_machine_name = join("", [local.client_name_machine_name, "-", local.project_machine_name])

  # Default tags
  default_tags = {
    client          = local.client_name
    client-project  = local.project_name
    client-billable = "Yes"
  }

  # Office CIDR
  office_cidr = "156.67.241.164/32"

  # AWS
  aws_account_id        = "193471113837"
  aws_region            = "eu-west-1"
  aws_availability_zone = "eu-west-1a"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

# Find the subnet ID for the matching aws_availability_zone
data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.default.id
  filter {
    name   = "availability-zone"
    values = [local.aws_availability_zone]
  }
}
