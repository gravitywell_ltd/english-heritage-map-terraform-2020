output "client_name" {
  value = local.client_name
}

output "client_name_machine_name" {
  value = local.client_name_machine_name
}

output "project_name" {
  value = local.project_name
}

output "project_machine_name" {
  value = local.project_machine_name
}

output "client_project_machine_name" {
  value = local.client_project_machine_name
}

output "client_project_short_machine_name" {
  value = local.client_project_short_machine_name
}

output "default_tags" {
  value = local.default_tags
}

output "office_cidr" {
  value = local.office_cidr
}

output "aws_account_id" {
  value = local.aws_account_id
}

output "aws_region" {
  value = local.aws_region
}

output "aws_vpc_id" {
  value = data.aws_vpc.default.id
}

output "aws_subnet_ids" {
  value = data.aws_subnet_ids.default.ids
}

output "aws_subnet_id" {
  value = sort(data.aws_subnet_ids.selected.ids)[0]
}

output "aws_availability_zone" {
  value = local.aws_availability_zone
}
