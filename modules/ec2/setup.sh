#!/bin/bash

sudo yum update -y
sudo swapoff -a
sudo dd if=/dev/zero of=/var/swapfile bs=1M count=2000
sudo chmod 600 /var/swapfile sudo mkswap /var/swapfile
sudo echo /var/swapfile none swap defaults 0 0 | sudo tee -a /etc/fstab
sudo swapon -a
sudo mv /etc/localtime /etc/localtime.bak
sudo ln -s /usr/share/zoneinfo/Europe/London /etc/localtime
sudo yum install -y httpd24
sudo yum install -y mod24_ssl
sudo yum install -y mysql57
sudo yum install -y mysql57-server
sudo yum install -y php72
sudo yum install -y php72-gd
sudo yum install -y php72-mcrypt
sudo yum install -y php72-opcache
sudo yum install -y php72-mbstring
sudo yum install -y php72-mysqlnd
sudo service httpd start
sudo chkconfig httpd on
sudo service mysqld start
sudo chkconfig mysqld on
sudo groupadd www
sudo usermod -a -G www ec2-user
sudo usermod -a -G www apache
sudo chown -R root:www /var/www
sudo chmod -R 775 /var/www
sudo find /var/www/ -type f -exec chmod 664 {} \;
sudo find /var/www/ -type d -exec chmod 775 {} \;
sudo yum install -y git
cd /etc/ && sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/composer
echo "test" >/etc/text.txt
