locals {
  security_group_name      = join("", [module.defaults.client_name, " - ", module.defaults.project_name, " EC2 (", var.environment, ")"])
  ec2_secrets_manager_name = join("", [module.defaults.client_project_machine_name, "-", var.environment, "-ec2-key-pair"])
  ec2_instance_name        = join("", [module.defaults.client_name, " - ", module.defaults.project_name, " (", var.environment, ")"])
  ec2_instance_count       = 1
  ec2_instance_class       = "t3.micro"
  ec2_instance_ami         = "ami-00890f614e48ce866" # Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type
}

module "defaults" {
  source = "../defaults"
}

#
# EC2 security group
#
module "ec2_instance_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = ">= 3.4.0"

  name        = local.security_group_name
  description = local.security_group_name
  vpc_id      = module.defaults.aws_vpc_id

  ingress_with_cidr_blocks = [
    {
      description = "SSH GW office access"
      protocol    = "tcp"
      from_port   = 22
      to_port     = 22
      cidr_blocks = module.defaults.office_cidr
    },
    {
      description = "HTTP access"
      protocol    = "tcp"
      from_port   = 80
      to_port     = 80
      cidr_blocks = "0.0.0.0/0"
    },
    {
      description = "HTTPS access"
      protocol    = "tcp"
      from_port   = 443
      to_port     = 443
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  computed_ingress_with_source_security_group_id = [
    for security_group in var.security_groups :
    {
      description              = security_group.description
      protocol                 = security_group.protocol
      from_port                = security_group.from_port
      to_port                  = security_group.to_port
      source_security_group_id = security_group.id
    }
  ]

  number_of_computed_ingress_with_source_security_group_id = length(var.security_groups)

  egress_with_cidr_blocks = [
    {
      description = "All traffic"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

#
# EC2 key-pair
#
resource "tls_private_key" "ec2_tls" {
  algorithm = "RSA"
}

resource "aws_key_pair" "ec2_key_pair" {
  key_name   = local.ec2_instance_name
  public_key = tls_private_key.ec2_tls.public_key_openssh
}

#
# AWS secrets
#
resource "aws_secretsmanager_secret" "ec2" {
  name                    = local.ec2_secrets_manager_name
  recovery_window_in_days = 0
  tags                    = module.defaults.default_tags
}

resource "aws_secretsmanager_secret_version" "ec2" {
  secret_id = aws_secretsmanager_secret.ec2.id
  secret_string = jsonencode({
    name            = local.ec2_instance_name
    private_key_pem = tls_private_key.ec2_tls.private_key_pem
  })
}

data "aws_secretsmanager_secret" "ec2" {
  depends_on = [aws_secretsmanager_secret.ec2]
  name       = local.ec2_secrets_manager_name
}

data "aws_secretsmanager_secret_version" "ec2" {
  secret_id = data.aws_secretsmanager_secret.ec2.id
}

#
# EC2 configuration script
#
resource "local_file" "ec2" {
  filename = join("/", [path.module, "setup.sh"])
}

data "local_file" "ec2" {
  filename = join("/", [path.module, "setup.sh"])
}

#
# EC2 Cluster
#
module "ec2_cluster" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = ">= 2.13.0"

  name           = local.ec2_instance_name
  instance_count = local.ec2_instance_count
  instance_type  = local.ec2_instance_class
  key_name       = jsondecode(data.aws_secretsmanager_secret_version.ec2.secret_string)["name"]

  # Instance setup
  ami                         = local.ec2_instance_ami
  cpu_credits                 = "unlimited"
  associate_public_ip_address = true
  ebs_optimized               = true

  # Monitoring
  monitoring = var.environment == "production" ? true : false

  # VPC subnet config
  subnet_id  = module.defaults.aws_subnet_id
  subnet_ids = module.defaults.aws_subnet_ids

  # Security group
  vpc_security_group_ids = [module.ec2_instance_sg.this_security_group_id]

  # Tags
  tags        = module.defaults.default_tags
  volume_tags = module.defaults.default_tags

  # Termination protection
  disable_api_termination = true

  # Instance configuration
  user_data = data.local_file.ec2.content
}

#
# Elastic IP association
#
resource "aws_eip" "ec2" {
  # depends_on = [module.ec2_cluster]
  count    = local.ec2_instance_count
  instance = module.ec2_cluster.id[count.index]
  vpc      = true
  tags     = merge(module.defaults.default_tags, { Name = local.security_group_name })
}
