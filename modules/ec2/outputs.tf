output "availability_zone" {
  value = module.ec2_cluster.availability_zone
}

output "public_ip" {
  value = aws_eip.ec2.*.public_ip
}

output "security_group_id" {
  value = module.ec2_instance_sg.this_security_group_id
}

output "security_group_name" {
  value = local.security_group_name
}
