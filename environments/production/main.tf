terraform {
  required_version = ">= 0.12.23"

  backend "remote" {
    organization = "Gravitywell"

    workspaces {
      name = "english-heritage-voe-map-production"
    }
  }

  required_providers {
    aws    = ">= 2.57.0"
    local  = ">= 1.4.0"
    random = ">= 2.2.1"
    tls    = ">= 2.1.1"
  }
}

module "core" {
  source      = "../../modules/core"
  environment = "production"
}
