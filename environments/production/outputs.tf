output "ec2_availability_zone" {
  value = module.core.ec2_availability_zone
}

output "ec2_public_ip" {
  value = module.core.ec2_public_ip
}

output "ec2_security_group_id" {
  value = module.core.ec2_security_group_id
}

output "ec2_security_group_name" {
  value = module.core.ec2_security_group_name
}

output "rds_availability_zone" {
  value = module.core.rds_availability_zone
}

output "rds_security_group_id" {
  value = module.core.rds_security_group_id
}

output "rds_instance_endpoint" {
  value = module.core.rds_instance_endpoint
}

output "rds_instance_db_name" {
  value = module.core.rds_instance_db_name
}

output "rds_instance_username" {
  value = module.core.rds_instance_db_name
}

output "rds_instance_port" {
  value = module.core.rds_instance_port
}
